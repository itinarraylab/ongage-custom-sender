## Plugin Description

This plugin is created to use Ongage API instead of **Wordpress native wp_mail** function. Before using it, you should make all necessary configurations in your Ongage Account: create new list, add new user,  etc.

## Plugin Configuration:

Settings are stored in database options table in the format of array. After deactivation of plugin all settings removed from database.

List of parameters needed from Ongage:

+ Account Code
+ Ongage Username
+ Ongage Password
+ ESP ID or ESP Connection ID
+ Mailing List ID
+ List ID

Also you need to set up:

+ Name From
+ Email Address From
+ Email Address Reply

For developers there is an option:

+ Debug Mode - output stores in error log and it is not recommended to have it enabled all the time. 



## Motivation

Inspired by Mandrill Plugin this plugin was created to allow the use of Ongage account to send transactional emails on your Wordpress online store. It also sends emails upon registration. In the next version we are planning to add option add new customers to your email list in Ongage.

## Installation

You can clone it from repository in your wp-content/plugins folder: **git clone https://gitlab.com/itinarraylab/ongage-custom-sender.git** or simply upload folder ongage-custom-sender to  wp-content/plugins using FTP.
There is no default settings for this plugin and you should configure it first. See section Plugin Configuration.

## API Reference

Plugin is based on Ongage Transactions API documentation. [Please read it for more information:] (https://apidocs.ongage.net/class-Controller_API_Notify_Transactions.html)

## Tests

There are no tests for these plugins.

## Contributors

This project was done in ITinArrayLab. **Author: Constantine Alexeyev**

## License

This project is subject to  [GNU GENERAL PUBLIC LICENSE, version 2](http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)