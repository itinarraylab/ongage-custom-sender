<?php

class Ongage{

    //Ongage email system definitions

    const ONGAGE_URL = 'https://api.ongage.net/';

    static $ongage_debug;
    static $account_code;
    static $username;
    static $password;

    function __construct($settings)
    {

        self::$ongage_debug=$settings['debug_mode'];
        self::$username=$settings['username'];
        self::$account_code=$settings['account_code'];
        self::$password=$settings['password'];



    }

    public function postRequest($request, $link, $method='post') {

        $debug = self::$ongage_debug;
        $link = self::ONGAGE_URL . $link;
        $request_json = json_encode($request);
        $c = curl_init();


        switch ($method) {
            case "post":
                curl_setopt($c, CURLOPT_URL, $link);
                curl_setopt($c, CURLOPT_POST, TRUE);
                curl_setopt($c, CURLOPT_POSTFIELDS, $request_json);
                break;
            case "put":
                curl_setopt($c, CURLOPT_URL, $link);
                curl_setopt($c, CURLOPT_PUT, TRUE);
                curl_setopt($c, CURLOPT_POSTFIELDS, $request_json);
                break;
            case "get":
                if (!empty($request)) {
                    $link .= '?' . http_build_query($request);
                }
                curl_setopt($c, CURLOPT_URL, $link);
                break;
        }
        $headers = array(
            'X_USERNAME: ' . self::$username,
            'X_PASSWORD: ' . self::$password,
            'X_ACCOUNT_CODE: ' .self:: $account_code,
        );
        curl_setopt($c, CURLOPT_HTTPHEADER, array_merge(array(
            // Overcoming POST size larger than 1k wierd behaviour
            // @link  http://www.php.net/manual/en/function.curl-setopt.php#82418
            'Expect:'), $headers
        ));
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        $response_raw = curl_exec($c);
        $errno = curl_errno($c);
        $result = json_decode($response_raw);
        if ($debug) {
            $log = "Request and Result Debug Ongage Custom Plugin:".PHP_EOL;
            $log.="url: " . $link .PHP_EOL;
            $log.="json: " . $request_json . PHP_EOL;
            $log.="method: " . $method . PHP_EOL;
            $log.="------------------------".PHP_EOL;
            $log.="Response: ".PHP_EOL;
            $log.= "json<: " . $response_raw . PHP_EOL;
            $log.="error: " . $errno . PHP_EOL."+++++++++++++++++++++++".PHP_EOL;
            error_log($log);
        }
        if (empty($errno)) {
            if (!empty($result->payload->errors)) {
                $errno = 500;
            }
        }
        if (!empty($errno)) {
            header("HTTP/1.0 " . $errno);
        }

        return $response_raw;
    }

}