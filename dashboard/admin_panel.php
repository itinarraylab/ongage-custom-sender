<?php
/**
 * Created by PhpStorm.
 * User: const
 * Date: 2016-10-14
 * Time: 1:13 PM
 */

add_action('admin_menu', 'ongage_admin_menu');

function ongage_admin_menu()
{


    add_options_page(
        __('Ongage Settings', 'wpongage'),
        __('Ongage Settings', 'wpongage'),
        'manage_options',
        'wpongage',
        'ongage_show_options_page'
    );
}

function ongage_show_options_page()
{

    if (!current_user_can('manage_options'))
        wp_die(__('You do not have sufficient permissions to access this page.'));


    $test_result = "";$test_subscribe_result="";
    if (count($_POST) > 0 && isset($_POST['onage_settings'])) {

        $prefix = 'ongage_';
        $options = ['ongage'];
        foreach ($options as $opt) {
            delete_option($prefix . $opt);
            if (isset($_POST[$opt]))
                add_option($prefix . $opt, $_POST[$opt]);
        }

    }

    if (count($_POST) > 0 && isset($_POST['ongage_send_test'])) {

        $email = ongage_get_test_email();
        if ($email) {
            $test_result = ongageCustomPlugin::sendTest('const.ca@gmail.com');
        } else {
            $test_result = 'Email cannot be empty value';
        }


    }

    if (count($_POST) > 0 && isset($_POST['ongage_send_subscribe_test'])) {

        $email = ongage_get_test_subscribe_email();
        $fname = ongage_get_test_subscribe_fname();
        $lname= ongage_get_test_subscribe_lname();
        if ($email!='' && $fname!='' && $lname!='') {
            $result = ongageCustomPlugin::subscribe($email, $fname, $lname, true);
            $test_subscribe_result = json_encode($result);
        } else {
            $test_subscribe_result = 'All Fields Are Mandatory';
        }


    }


    ?>
    <style>
        .button-secondary {
            background: #ff0000 !important;
            color: #ffffff !important;
        }

        .message_test {
            min-height: 50px;
            width: 100%
        }
    </style>
    <div class="wrap">
        <h1>Ongage Settings</h1>
        <form method="post">
            <fieldset>
                <legend><strong>Ongage Account Settings</strong></legend>
                <table class="form-table">


                    <tr>
                        <th><label for="ongage_account_code">Account Code</label></th>
                        <td>
                            <input name="ongage[account_code]" type="text" id="ongage_account_code"
                                   value="<?= ongage_get_array_option('ongage', 'account_code') ?>"
                                   class="regular-text" placeholder="Enter Ongage Account Code" autocomplete="off"/>
                        </td>
                    </tr>

                    <tr>
                        <th><label for="ongage_username">Username</label></th>
                        <td>
                            <input name="ongage[username]" type="email" id="ongage_username"
                                   value="<?= ongage_get_array_option('ongage', 'username') ?>"
                                   class="regular-text" autocomplete="off" placeholder="Enter Ongage Username"/>
                        </td>
                    </tr>

                    <tr>
                        <th><label for="ongage_password">Password</label></th>
                        <td>
                            <input name="ongage[password]" type="password" id="ongage_password"
                                   value="<?= ongage_get_array_option('ongage', 'password') ?>"
                                   class="regular-text" autocomplete="off" placeholder="Enter Ongage Password"/>
                        </td>
                    </tr>

                    <tr>
                        <th><label for="ongage_esp_id">ESP ID (ESP Connection ID)</label></th>
                        <td>
                            <input name="ongage[esp_id]" type="text" id="ongage_esp_id"
                                   value="<?= ongage_get_array_option('ongage', 'esp_id') ?>"
                                   class="regular-text" autocomplete="off" placeholder="Enter Ongage ESP ID"/>
                        </td>
                    </tr>

                    <tr>
                        <th><label for="ongage_mailing_id">Transactional Campaign Id</label></th>
                        <td>
                            <input name="ongage[mailing_id]" type="text" id="ongage_mailing_id"
                                   value="<?= ongage_get_array_option('ongage', 'mailing_id') ?>"
                                   class="regular-text" autocomplete="off" placeholder="Transactional Campaigns (e.g Default Transactional Campaign Id)  "/>
                        </td>
                    </tr>

                    <tr>
                        <th><label for="ongage_list_id">List Id</label></th>
                        <td>
                            <input name="ongage[list_id]" type="text" id="ongage_list_id"
                                   value="<?= ongage_get_array_option('ongage', 'list_id') ?>"
                                   class="regular-text" autocomplete="off" placeholder="List Id"/>
                        </td>
                    </tr>
                </table>
            </fieldset>
                    <fieldset>
                        <legend><strong>Sender Settings: </strong></legend>
                        <table class="form-table">

                    <tr>
                        <th><label for="ongage_from_name">FROM Name</label></th>
                        <td>
                            <input name="ongage[from_name]" type="text" id="ongage_from_name"
                                   value="<?= ongage_get_array_option('ongage', 'from_name') ?>"
                                   class="regular-text" autocomplete="off" placeholder="Name From"/>
                        </td>
                    </tr>

                    <tr>
                        <th><label for="ongage_from_address">FROM Email</label></th>
                        <td>
                            <input name="ongage[from_address]" type="email" id="ongage_from_address"
                                   value="<?= ongage_get_array_option('ongage', 'from_address') ?>"
                                   class="regular-text" autocomplete="off" placeholder="Email Address From"/>
                        </td>
                    </tr>

                    <tr>
                        <th><label for="ongage_reply_address">Reply-To Email</label></th>
                        <td>
                            <input name="ongage[reply_address]" type="email" id="ongage_reply_address"
                                   value="<?= ongage_get_array_option('ongage', 'reply_address') ?>"
                                   class="regular-text" autocomplete="off" placeholder="Email Address Reply"/>
                        </td>
                    </tr>
                </table>
            </fieldset>

            <fieldset>
                <legend><strong>User Registration: </strong></legend>
                <table class="form-table">

                    <tr valign="top">
                        <th scope="row"><label for="ongage_subscribe_mode">Subscribe New Users</label></th>
                        <td>
                            <input name="ongage[subscribe_mode]" type="checkbox" id="ongage_subscribe_mode"
                                <?php echo ongage_get_array_checkbox_option('ongage', 'subscribe_mode'); ?>
                            />
                            Check this to subscribe new users to email list.
                        </td>
                    </tr>
                </table>
            </fieldset>

            <fieldset>
                <legend><strong>Developer Tools: </strong></legend>
                <table class="form-table">

                    <tr valign="top">
                        <th scope="row"><label for="ongage_debug_mode">Debug Mode</label></th>
                        <td>
                            <input name="ongage[debug_mode]" type="checkbox" id="ongage_debug_mode"
                                <?php echo ongage_get_array_checkbox_option('ongage', 'debug_mode'); ?>
                            />
                            Check this to enable debug mode. (Output sends to error log)
                        </td>
                    </tr>
                </table>
            </fieldset>


            <?= ongage_print_button('Save Changes'); ?>

        </form>
        <fieldset id="test_box">
            <form method="post" action="#test_box">

                <table class="form-table">
                    <tr>
                        <th><label for="ongage_test_email">Test Email</label></th>
                        <td>
                            <input name="ongage_test_email" type="email" id="ongage_test_email"
                                   value="<?= ongage_get_test_email() ?>"
                                   class="regular-text" autocomplete="off" placeholder="Add Your Email To Receive a Test"/>
                        </td>
                    </tr>

                </table>


                <?= ongage_print_button('Send Test Email', 'ongage_send_test', 'button-secondary'); ?>
                <div class="message_test">

                    <p>
                        <?= $test_result; ?>
                    </p>
                </div>
            </form>

        </fieldset>

        <fieldset id="test_subscribe_box">
            <form method="post" action="#test_subscribe_box">

                <table class="form-table">
                    <tr>
                        <th><label for="ongage_test_subscribe_email">Test Subscribe Email</label></th>
                        <td>
                            <input name="ongage_test_subscribe_email" type="email" id="ongage_test_subscribe_email"
                                   value="<?= ongage_get_test_subscribe_email() ?>"
                                   class="regular-text" autocomplete="off" placeholder="Add New Email To Test Subscription"/>
                        </td>
                    </tr>
                    <tr>
                        <th><label for="ongage_test_subscribe_fname">Test Subscribe First Name</label></th>
                        <td>
                            <input name="ongage_test_subscribe_fname" type="text" id="ongage_test_subscribe_fname"
                                   value="<?= ongage_get_test_subscribe_fname() ?>"
                                   class="regular-text" autocomplete="off" placeholder="Add New  First Name To Test Subscription"/>
                        </td>
                    </tr>
                    <tr>
                        <th><label for="ongage_test_subscribe_lname">Test Subscribe Last Name</label></th>
                        <td>
                            <input name="ongage_test_subscribe_lname" type="text" id="ongage_test_subscribe_lname"
                                   value="<?= ongage_get_test_subscribe_lname() ?>"
                                   class="regular-text" autocomplete="off" placeholder="Add New Last Name To Test Subscription"/>
                        </td>
                    </tr>

                </table>

                <?= ongage_print_button('Subscribe Test Email', 'ongage_send_subscribe_test', 'button-secondary'); ?>
                <div class="message_test">

                    <p>
                        <?= $test_subscribe_result; ?>
                    </p>
                </div>
            </form>

        </fieldset>
    </div>
    <?php

}



function ongage_get_array_option($key, $value, $prefix = 'ongage_')
{
    $key = $prefix . $key;
    return isset(get_option($key)[$value]) ? get_option($key)[$value] : '';
}

function ongage_get_array_checkbox_option($key, $value, $prefix = 'ongage_')
{
    $key = $prefix . $key;
    return isset(get_option($key)[$value]) ? (get_option($key)[$value] == 'on') ? 'checked' : '' : '';


}


function ongage_print_button($value, $hidden_name='onage_settings', $btn_class='button-primary')
{



    $btn = "<p class=\"submit\">" . PHP_EOL;
    $btn .= "<input type=\"submit\" name=\"Submit\" class=\"button $btn_class\" value=\"$value\" />" . PHP_EOL;
    $btn .= "<input type=\"hidden\" name=\"$hidden_name\" value=\"save\" style=\"display:none;\" />" . PHP_EOL;
    $btn .= "</p>" . PHP_EOL;
    return $btn;


}

function ongage_get_test_email()
{
    return isset($_POST['ongage_test_email']) ? $_POST['ongage_test_email'] : '';
}

function ongage_get_test_subscribe_email()
{
    return isset($_POST['ongage_test_subscribe_email']) ? $_POST['ongage_test_subscribe_email'] : '';
}
function ongage_get_test_subscribe_fname()
{
    return isset($_POST['ongage_test_subscribe_fname']) ? $_POST['ongage_test_subscribe_fname'] : '';
}
function ongage_get_test_subscribe_lname()
{
    return isset($_POST['ongage_test_subscribe_lname']) ? $_POST['ongage_test_subscribe_lname'] : '';
}

function print_test_result($result = null)
{
    if (!is_null($result)) {

        return $result;
    }

}